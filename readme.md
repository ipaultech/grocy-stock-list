# Grocy Stock List

Ich habe dieses Projekt erstellt, um den aktuellen Lagerfüllstand. Meiner Nahrungsmittelvorräte auf einen Blick zu sehen.

Da Grocy doch in dieser weiße sehr eingeschränkt war und ich schnell eine genaue Kilogramm Anzahl sehen muss.
Dieses Projekt fragt die API von Grocy via PHP + CURL ab und Listet mir die sinnvollen Werte auf.

![Screenshot #1](/screenshot1.png)

[Grocy Projekt](https://grocy.info/)
[Grocy API Dokumentation](https://demo.grocy.info/api)

# Installation

Das Projekt lässt sich schnell und einfach per Docker-Compose deployen.
Eine Anleitung erspare ich mir hier erstmal.
