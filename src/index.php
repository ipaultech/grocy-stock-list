<?php
#Created by BERZ.DEV in 2023
#A working curl module is required

#Error Handling
error_reporting(E_ERROR | E_PARSE);

#Vars
include 'vars.php';

$apikey = $grocyapikey;
$apihost = $grocyhost;
#Define the quantity unit ids:
#Use: /api/objects/quantity_units for getting the values
$kgunit = $quantityid_kg;
$grammunit = $quantityid_gramm;


#Function for creating list items
function tv($value) {
    echo '<td>' .$value .'</td>';
}

#Function to call the API via CURL
function callAPI($method, $url, $data){
    $curl = curl_init();
    switch ($method){
       case "POST":
          curl_setopt($curl, CURLOPT_POST, 1);
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
          break;
       case "PUT":
          curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
          if ($data)
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
          break;
       default:
          if ($data)
             $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    #Get Variables
    global $apikey;
    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
       $apikey,
       'accept: application/json',
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // EXECUTE:
    $result = curl_exec($curl);
    if(!$result){die("Connection Failure");}
    curl_close($curl);
    return $result;
 }

$get_data = callAPI('GET', $apihost .'/api/objects/locations', false);
$response_stock = json_decode($get_data, true);

?>

<html>
<head>
<title>Stock List</title>
<link rel="shortcut icon" href="ic/favicon.ico" type="image/x-icon">
<link rel="icon" href="ic/favicon32.png" sizes="32x32">
<link rel="icon" href="ic/favicon48.png" sizes="48x48">
<link rel="icon" href="ic/favicon96.png" sizes="96x96">

<script src="/DataTables/jquery-3.6.3.min.js"></script>
<script src="/DataTables/datatables.js"></script>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

ul {
    font-size: 30px;
}

</style>
</head>
<body>

<h2>Lager Übersicht</h2>

<table id="table1" class="display" style="width:100%">
  <thead>
  <tr>
    <th>LTAG</th>
    <th>Ungenau</th>
    <th>KG SUM</th>
    <th>Artikel COUNT</th>
    <th>Artikel Liste</th>
  </tr>
  </thead>
  <tbody>

<?php

$rowcount = 0;
$gesamtgewicht = 0;
$itemanzahl = 0;
$lageranzahl = 0;
$row_array = array();

foreach($response_stock as $key => $val) {
    $rowcount += 1;
    $lageranzahl += 1;

    #Define Values
    $stock_name = $val[name];
    $stock_id = $val[id];
    $stock_description = $val[description];
    $stock_gewicht_gesamt = 0;
    $stock_gewicht_ungenau = 0;
    $stock_item_anzahl = 0;
    $stock_item_liste = array();

    #Get each Item in this Stock Location
    $get_data = callAPI('GET', $apihost .'/api/stock/locations/' .$stock_id .'/entries', false);
    $response_location_entries = json_decode($get_data, true);

    #Get Name and amount of the item
    foreach($response_location_entries as $key => $val) {
        $stock_item_anzahl += 1;
        #Define Product Details in Variables
        $location_entrie_product_id = $val[product_id];
        $location_entrie_amount = $val[amount];

        #Get Product Details
        $get_data = callAPI('GET', $apihost .'/api/stock/products/' .$location_entrie_product_id, false);
        $response_product = json_decode($get_data, true);
        
        #Define more Product Details in Variables
        $entrie_item_name = $response_product['product']['name'];
        $entrie_item_quantity_unit_id = $response_product['quantity_unit_stock']['id'];
        $entrie_item_quantity_unit_name = $response_product['quantity_unit_stock']['name'];

        #If quantity unit is the gramm id then convert to kg
        if ( $entrie_item_quantity_unit_id == $grammunit ) {
            $entrie_item_gewicht = ($location_entrie_amount / 1000);
            $stock_gewicht_gesamt += $entrie_item_gewicht;
            array_push($stock_item_liste, $entrie_item_name);
        }

        #If the quantity untis is kg then do only attach it
        elseif ( $entrie_item_quantity_unit_id == $kgunit ) {
            $entrie_item_gewicht = $location_entrie_amount;
            $stock_gewicht_gesamt += $entrie_item_gewicht;
            array_push($stock_item_liste, $entrie_item_name);
        }

        #If the quantity is a other value then ignore it...
        else {
            $stock_gewicht_ungenau += 1;
            array_push($stock_item_liste, $entrie_item_name .' ' .$location_entrie_amount .' ' .$entrie_item_quantity_unit_name);
        }

    }

    #This is only for styling the row colours...
    if ($rowcount % 2 != 0) {
        $tableanfang = '<tr>';
        }
    else {
        $tableanfang = '<tr bgcolor="#dddddd">';
    }

    #Färbe ungenaue Lager Standorte
    if ( $stock_gewicht_ungenau != 0 ) {
        if ($rowcount % 2 != 0) {
            $tableanfang = '<tr bgcolor="#ff9966">';
            }
        else {
            $tableanfang = '<tr bgcolor="#ffcc66">';
        }
    }

    #Printe Reihe....
    echo $tableanfang;
    tv($stock_name);
    tv($stock_gewicht_ungenau);
    tv($stock_gewicht_gesamt);
    tv($stock_item_anzahl);

    #Liste Items auf...
    foreach($stock_item_liste as $result) {
        $string_itemliste .= $result;
        $string_itemliste .= ' - ';
    }

    #Printe sie...
    tv($string_itemliste);
    $string_itemliste = null;
    echo '</tr>';

    #Ein paar Stats...
    $gesamtgewicht += $stock_gewicht_gesamt;
    $itemanzahl += $stock_item_anzahl;
    $row_array = array_merge($row_array, [$stock_name => $stock_gewicht_gesamt]);
}

echo '</tbody>';
echo '</table>';

echo '<h3>' .$gesamtgewicht .' kg Gesamtgewicht.</h3>';
echo '<h3>Und ' .$itemanzahl .' Slots auf ' .$lageranzahl .' unterschiedlichen Lagerpositionen.</h3>';
?>

<script>
$(document).ready(function () {
    $('#table1').DataTable();
});
</script>
</body>
</html>