<?php
#This File includes runtime vars, that'll be used globally in all scripts.
#RUNTIME VARS

//GROCY HOSTNAME
$grocyhost = getenv('GROCYHOST');

//GROCY API KEY
$grocyapikey = getenv('GROCYAPIKEY');

//Quantity IDs
$quantityid_kg = getenv('GROCYQUANTITYIDKG');
$quantityid_gramm = getenv('GROCYQUANTITYIDGRAMM');
?>